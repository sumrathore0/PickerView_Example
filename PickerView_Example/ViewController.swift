//
//  ViewController.swift
//  PickerView_Example
//
//  Created by sadhna karsoliya on 30/06/18.
//  Copyright © 2018 sadhna karsoliya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var pickerViewOutlet: UILabel!
    
    var PickerViewObj : PickerView?
    var pickerArray : NSArray = ["Afghanistan", "Albania" ,"Algeria" ,"Andorra","Australia","Belgium","INDIA","PAKISTAN"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func pressAction(_ sender: UIButton) {
        showPicker()
    }
   
    func showPicker() {
        PickerViewObj = PickerView(view: self.view, delegate: self, dateSource: self)
        PickerViewObj?.show()
    }

}

extension ViewController : PickerViewModelDatasource {
    
    func getTitle(titleForRow row: Int, forComponent component: Int) -> String {
         return pickerArray[row] as! String
    }
    
    func getCount(numberOfRowsInComponent component: Int) -> Int {
        return (pickerArray.count)
    }
}

extension ViewController : PickerViewDelegate {
    func didCancelPickerView() {
        PickerViewObj?.hide()
    }
    
    func didSelectPickerView(index: Int) {
        pickerViewOutlet.text = self.pickerArray[index] as! String
    }
}

