//
//  PickerView.swift
//  demooo
//
//  Created by sadhna karsoliya on 01/06/18.
//  Copyright © 2018 asd. All rights reserved.
//

import UIKit

protocol PickerViewDelegate {
    func didSelectPickerView(index : Int)
    func didCancelPickerView()
}

protocol PickerViewModelDatasource{
    func getTitle(titleForRow row: Int, forComponent component: Int) -> String
    func getCount(numberOfRowsInComponent component: Int) -> Int
}

protocol PickerViewModelDelgate{
    func didSelected(object : Any)
}

class PickerView: NSObject {
    
    var picker: UIPickerView?
    var delegate: PickerViewDelegate?
    weak var view: UIView?
    var overlayView: UIView?
    var doneButtonView: UIView?
    var dateSource: PickerViewModelDatasource?
    var selectedDictionaryValue: Int = 0

    init(view: UIView , delegate : PickerViewDelegate , dateSource : PickerViewModelDatasource) {
        super.init()
        self.view = view
        self.delegate = delegate
        self.dateSource = dateSource
    }
    
    func show(){
        if picker == nil {
            picker = UIPickerView()
            picker?.frame = CGRect(x: 0, y: (self.view?.frame.size.height)! - 240 , width: (self.view?.frame.size.width)! , height: 240)
            picker?.backgroundColor = UIColor.white
            picker?.showsSelectionIndicator = true
            picker?.delegate = self
            picker?.dataSource = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            //toolBar.isTranslucent = true
            //toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
            toolBar.sizeToFit()
            
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelButtonAction))
            
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            
            doneButtonView = UIView()
            doneButtonView?.frame = CGRect(x: 0, y: (self.view?.frame.size.height)! - 280 , width: (self.view?.frame.size.width)! , height: 40)
            doneButtonView?.backgroundColor = UIColor.black
            doneButtonView?.addSubview(toolBar)
            
            var rect_overlay = (self.view?.frame)!
            rect_overlay.origin.y = 0
            overlayView = UIView(frame: (rect_overlay))
            overlayView?.backgroundColor = UIColor.init(white: 0, alpha: 0.2)
            self.view?.addSubview(overlayView!)
            self.view?.addSubview(picker!)
            self.view?.addSubview(doneButtonView!)
        }
    }
    
    @objc func doneButtonAction(){
        self.hide()
        self.overlayView?.removeFromSuperview()
        self.delegate?.didSelectPickerView(index : selectedDictionaryValue)
    }
    
    @objc func cancelButtonAction(){
        self.delegate?.didCancelPickerView()
    }

    func hide(){
        self.overlayView?.removeFromSuperview()
        picker?.removeFromSuperview()
        doneButtonView?.removeFromSuperview()
    }
}

extension PickerView : UIPickerViewDelegate , UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.dateSource!.getCount(numberOfRowsInComponent: component)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = self.dateSource?.getTitle(titleForRow: row, forComponent: component)
        return title
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedDictionaryValue  = row
    }
}
